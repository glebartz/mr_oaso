# Mr_OASO

**Medical resarch Open Access Science Observatory**

> TODO: update once we know more of what will be going on here.


## Draft content:

> You will need a running instance of Grobid at a given moment.
> You will need llama cpp https://github.com/ggerganov/llama.cpp  It is also possible to use transformers library if you prefer. But i'd rather not use it at first.

1. Everything is flatten here, no subfolder, because x)
2. Two ipynb notebooks : 
    - **RAG_test** is used to test RAGs through Grobid pdf extraction
    - **get_pdfs_from_doi**, gets pdfs from DOIS requesting Unpaywall API
3. csvs are extracted from the 300 pdfs annoted databse
4. pdfs are samples (not originated from the 300 pdfs databse) used for RAG testing
5. R Files are deprecated (or deleted so just ignore this line)
6. RTFM :D